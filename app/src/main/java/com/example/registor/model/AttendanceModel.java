package com.example.registor.model;

public class AttendanceModel {

    String groupA, groupB, groupC, groupD, groupE, groupT, id, name, mobilenumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupA() {
        return groupA;
    }

    public void setGroupA(String groupA) {
        this.groupA = groupA;
    }

    public String getGroupB() {
        return groupB;
    }

    public void setGroupB(String groupB) {
        this.groupB = groupB;
    }

    public String getGroupC() {
        return groupC;
    }

    public void setGroupC(String groupC) {
        this.groupC = groupC;
    }

    public String getGroupD() {
        return groupD;
    }

    public void setGroupD(String groupD) {
        this.groupD = groupD;
    }

    public String getGroupE() {
        return groupE;
    }

    public void setGroupE(String groupE) {
        this.groupE = groupE;
    }

    public String getGroupT() {
        return groupT;
    }

    public void setGroupT(String groupT) {
        this.groupT = groupT;
    }
}
